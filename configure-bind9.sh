#! /bin/bash

set -ex

# configure bind9 to forward all requests
cat << EOF > /etc/bind/named.conf.options
options {
        directory "/var/cache/bind";
        auth-nxdomain no;    # conform to RFC1035
     // listen-on-v6 { any; };
        listen-on port 53 { localhost; 192.168.0.0/24; };
        allow-query { localhost; 192.168.0.0/24; };
        forwarders { 8.8.8.8; };
        recursion yes;
        };
EOF

# setup zones
cat << EOF > /etc/bind/named.conf.local
zone "example.com" {
  type master;
  file "/etc/bind/zones/db.example.com";
} ;

zone "1.0.0.127.in-addr.arpa" {
  type master;
  file "/etc/bind/zones/db.1.0.0.127";
};
EOF

# configure zone example.com
mkdir -p /etc/bind/zones
cat << EOF > /etc/bind/zones/db.example.com
\$TTL	1
@	IN	SOA	ns1.example.com. admin.example.com. (
			     1	;<serial-number>
			     1	;<time-to-refresh>
			     1	;<time-to-retry>
			604800	;<time-to-expire>
			     1)	;<minimum-TTL>
;ListNameservers
	IN	NS	ns1.example.com.
;
ns1.example.com.	IN	A	127.0.0.1
example.com.	IN	A	127.0.0.1
www.example.com.	IN	CNAME   example.com.
_esni.example.com.     IN      TXT     "/wH/dAuzACQAHQAga69uFnbN0zhCdYwbSQcGBTu1rm0D9+2M5pNt24+suB4AAhMBAQQAAAAAXcqvKQAAAABdysRBAAA="
EOF

cat << EOF > /etc/bind/zones/db.1.0.0.127
\$TTL	1
@	IN	SOA	ns1.example.com. admin.example.com. (
			     1	;<serial-number>
		     1	;<time-to-refresh>
			     1	;<time-to-retry>
			604800	;<time-to-expire>
		     1)	;<minimum-TTL>
; name servers
	IN	NS	ns1.example.com.
;
1.0.0.127	IN	PTR	example.com.
1.0.0.127	IN	PTR	www.example.com.
1.0.0.127	IN	PTR	svc.example.com.
1.0.0.127	IN	PTR	svc2.example.com.
1.0.0.127	IN	PTR	svc3.example.com.
EOF
